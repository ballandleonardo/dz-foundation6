//* -----------------
//  GULPFILE.JS
//* -----------------

var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber');
    
//* -----------------
//  SASS TASKS
//* -----------------
gulp.task('sass', function() {
  return gulp.src('./assets/scss/**/*.scss')
    .pipe(plumber(function(error) {
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
    }))
    .pipe(sass())
    .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'],
            cascade: false
        }))
    .pipe(rename({basename: 'style'}))
    .pipe(gulp.dest('./assets/css/'))     
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('./assets/css/'))
});    
    
//* -----------------
//  JAVASCRIPT TASKS
//* -----------------
gulp.task('scripts', function() {
  return gulp.src([
      './assets/js/scripts/*.js'  		  
  ])
    .pipe(plumber())
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('./assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./assets/js'))
});    

gulp.task('libs', function() {
  return gulp.src([	
          './assets/libs/jquery/jquery.js',
          './assets/libs/what-input/what-input.js'          
  ])
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('./assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./assets/js'))
});

//* -----------------
//  FOUNDATION TASKS
//* -----------------

gulp.task('foundation', function() {
  return gulp.src([	
  		  
          './assets/libs/foundation/js/foundation.core.js',
          './assets/libs/foundation/js/foundation.util.*.js',
          './assets/libs/foundation/js/foundation.abide.js',
          './assets/libs/foundation/js/foundation.accordion.js',
          './assets/libs/foundation/js/foundation.accordionMenu.js',
          './assets/libs/foundation/js/foundation.drilldown.js',
          './assets/libs/foundation/js/foundation.dropdown.js',
          './assets/libs/foundation/js/foundation.dropdownMenu.js',
          './assets/libs/foundation/js/foundation.equalizer.js',
          './assets/libs/foundation/js/foundation.interchange.js',
          './assets/libs/foundation/js/foundation.magellan.js',
          './assets/libs/foundation/js/foundation.offcanvas.js',
          './assets/libs/foundation/js/foundation.orbit.js',
          './assets/libs/foundation/js/foundation.responsiveMenu.js',
          './assets/libs/foundation/js/foundation.responsiveToggle.js',
          './assets/libs/foundation/js/foundation.reveal.js',
          './assets/libs/foundation/js/foundation.slider.js',
          './assets/libs/foundation/js/foundation.sticky.js',
          './assets/libs/foundation/js/foundation.tabs.js',
          './assets/libs/foundation/js/foundation.toggler.js',
          './assets/libs/foundation/js/foundation.tooltip.js',
          './assets/libs/foundation/js/motion-ui.js'
  ])
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(concat('foundation.js'))
    .pipe(gulp.dest('./assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./assets/js'))
});  

//* -----------------
//  DEFAULT TASK
//* -----------------

gulp.task('default', function() {
  gulp.start('sass', 'scripts', 'libs', 'foundation');
});

//* -----------------
//  WATCH TASK
//* -----------------

gulp.task('watch', function() {

  gulp.watch('./assets/scss/**/*.scss', ['sass']);
  gulp.watch('./assets/js/scripts/*.js', ['scripts']);
  gulp.watch('./libs/foundation/*.js', ['foundation']);

});