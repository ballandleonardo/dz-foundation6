# README #

Welcome to the new Starterkit by Digitalz. It uses the really new ZURB Foundation6 to explore new ways of designing websites and web-apps.

### Informations ###

* DIGITALZ is a web digital company, working with clients from Brasil and France.
* Starterkit Version 0.1.0
* [Starterkit Homepage](https://bitbucket.org/leodsidz/dz-foundation6)

### How do I get set up? ###

* Download or Clone the repo

```
#!cmd

clone https://leodsidz@bitbucket.org/leodsidz/dz-foundation6.git
```

* Install the dev dependencies
```
#!cmd

npm install
```

* Initiate the assets files (scss compiling, js concat, minifying everything and more...)

```
#!cmd

gulp
```
* Live Rebuilding of the assets
```
#!cmd

gulp watch
```


### Contributions ###

* Big thanks to ZURB to develop this awesome framework!
* Starterkit inspired by JointsWP
* Don't hesitate to contact me if you got any questions.

### Who do I talk to? ###

* Leonardo da Silva - BR/FR
* digitalz4k@gmail.com